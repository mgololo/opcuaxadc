/* © Copyright CERN, Universidad de Oviedo, 2015.  All rights not expressly granted are reserved.
 * QuasarServer.cpp
 *
 *  Created on: Nov 6, 2015
 * 		Author: Damian Abalo Miron <damian.abalo@cern.ch>
 *      Author: Piotr Nikiel <piotr@nikiel.info>
 *
 *  This file is part of Quasar.
 *
 *  Quasar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public Licence as published by
 *  the Free Software Foundation, either version 3 of the Licence.
 *
 *  Quasar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public Licence for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Quasar.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "QuasarServer.h"
#include <boost/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <LogIt.h>
#include <thread>
#include <string.h>
#include <shutdown.h>

#include <DRoot.h>
#include <DXADC.h>
#include <DTemperatures.h>
#include <DVoltages.h>
//#include "/home/siyaya/quasar/Device/src/DVoltages.cpp"

#include <boost/foreach.hpp>

QuasarServer::QuasarServer() : BaseQuasarServer()
{

}

QuasarServer::~QuasarServer()
{
 
}

void QuasarServer::mainLoop()
{
    printServerMsg("Press "+std::string(SHUTDOWN_SEQUENCE)+" to shutdown server");

    // Wait for user command to terminate the server thread.

    while(ShutDownFlag() == 0)
    {
        //std::this_thread::sleep_for(std::chrono::milliseconds(100));
    	boost::this_thread::sleep(boost::posix_time::milliseconds(1000));

       	BOOST_FOREACH( Device::DXADC * board, Device::DRoot::getInstance()->xadcs() )
        	{
      			BOOST_FOREACH( Device::DTemperatures* sensor, board->temperaturess() )
                {
      				sensor -> update();
                }
      			BOOST_FOREACH( Device::DVoltages* voltage, board->voltagess() )
                {
      				voltage -> update();
                }
        	}
    }
    printServerMsg(" Shutting down server");
}

void QuasarServer::initialize()
{
    LOG(Log::INF) << "Initializing Quasar server.";

}

void QuasarServer::shutdown()
{
	LOG(Log::INF) << "Shutting down Quasar server.";
}

void QuasarServer::initializeLogIt()
{
	BaseQuasarServer::initializeLogIt();
    LOG(Log::INF) << "Logging initialized.";
}
